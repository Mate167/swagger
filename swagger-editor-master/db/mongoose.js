//mongose
const mongoose = require("mongoose");
const url = 'mongodb://localhost:27017/';
const dbName = 'galeryDB';
mongoose.connect(url+dbName, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});