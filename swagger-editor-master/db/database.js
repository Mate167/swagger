const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017';
const dbName = 'galeryDB';

MongoClient.connect(url, async function(err, client){
    assert.equal(null, err);
    console.log("Connected successfully to server");

    const db = client.db(dbName);

    ////insert
    // await db.collection('users').insertOne({
    //     {
            // _id: ObjectId( "619d1cdfd843a05e8b3c6d6e")
            // name: "Stanisław",
            // surname: "Programista"    
    // }, (error, result)=>{
    //     if(error){
    //         console.log("not ok");
    //         console.log(error)
    //     }
    //     else    
    //         console.log("working - all ok");
    // })

    ////select
    // db.collection('users').find({
    //     "name": { $eq: 'Stanisław'}
    // }).toArray((err,res)=>{
    //     console.log(res)
    // })


    ////update
    // db.collection('users').updateOne({
    //     name: "Tadeusz"
    // }, {
    //     $set:{
    //         name: "Tymoteusz"
    //     }
    // })

    ////delete
    // db.collection('users').deleteOne({
    //         name: "Stanisław"
    //     }, (err, res) => {
    //         if(err) 
    //             console.log("error");
    //         else
    //             console.log(res)
    //     })
});