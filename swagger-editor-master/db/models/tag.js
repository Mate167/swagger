const { ObjectId } = require("bson");
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let tag = new Schema({
    id: ObjectId,
    text: {
        type: String,
        required: true,
    },
})

const model = mongoose.model("Tag", tag);

module.exports = model;