const { ObjectId } = require("bson");
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let session = new Schema({
    id: ObjectId,
    token: {
       type: String,
       required: true,
    },
    expiredDate: {
        type: Date,
        required: true,
    },
})

const model = mongoose.model("Session", session);

module.exports = model;