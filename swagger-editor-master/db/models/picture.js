const { ObjectId } = require("bson");
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let picture = new Schema({
    id: ObjectId,
    tittle: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    date: {
        type: Date,
        required: true,
    },
    fileName: {
        type: String,
        required: true,
    },
    path: {
        type:String,
        default: './images'
    },
    size: {
        type:Number,
        required: true,
        max: 10000000,
    }
})

const model = mongoose.model("Picture", picture);

module.exports = model;

// const pictureInsert = new Picture({tittle: 'foto_test2021-11-30', description: 'My photo', date:'2021-11-30', fileName:'myPhoto.png', path:'./images', size: 12345});
// picture.save().then(() => {console.log(pictureInsert)}).catch(err => {console.log(err)});


// const getPictures = async () =>{
//     try{
//         const pictures = await Picture.find({
//             size: {$gt: 1234}
//         });
//         console.log(pictures);
//     }catch(error){
//         console.log(error);
//     }
// };
// getPictures();