const { ObjectId } = require("bson");
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let miniature = new Schema({
    id: ObjectId,
    typeOfMiniature: {
        type: String,
        default: 'None',
    },
})

const model = mongoose.model("Miniature", miniature);

module.exports = model;