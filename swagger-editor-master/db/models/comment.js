const { ObjectId } = require("bson");
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let comment = new Schema({
    id: ObjectId,
    text: {
        type: String,
        required: true,
    },
    date: {
        type: Date,
        required: true,
    },
})

const model = mongoose.model("Comment", comment);

module.exports = model;