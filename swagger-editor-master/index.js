const express = require('express')
const port = 8000
const bodyParser = require('body-parser')
const app = express()
const userRoutes = require('./routes/users.js')
const mongoose = require('./db/mongoose.js')
const Picture = require('./db/models/picture.js')
const Session = require('./db/models/session.js')
const Tag = require('./db/models/tag.js')
const User = require('./db/models/user.js')
const Comment = require('./db/models/comment.js')
const Gallery = require('./db/models/gallery.js')
const Miniature = require('./db/models/miniature.js')

app.use(bodyParser.json());
app.use('/users', userRoutes)
app.set('view engine', 'hbs')

const addPicture = async (pictureData) => {
    try{
        const picture = new Picture(pictureData)
        picture.save()
        console.log(picture)
    }catch(error){
        console.log(error)
    }
}
addPicture({
    tittle: 'foto_test2021-11-30 - vol.2',
    description: 'My photo2',
    date:'2021-11-30',
    fileName:'myPhoto.png', 
    size: 1235})

// app.get('/fromFile', (req,res) => {
//     res.sendFile(__dirname+'/index.html')
// })

// app.get('/', (req,res) => {
//     res.render('index', {
//         Title: "Galeria",
//         Body: "Zdjęcia"
//     })
// })

// app.get('/comments', (req,res) => {
//     res.send('komentarze')
// })

app.listen(port)