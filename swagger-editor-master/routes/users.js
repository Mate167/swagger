const express = require('express')

const router = express.Router();

const lastId = 3;
let users = [
    {
        id: 1,
        name:"Mateusz",
        surname: "Suchora",
        login: "MSuchora",
        email: "MSuchora@example.com",
        password: "tajneHasło",
        isAdmin: true,
    },
    {
        id: 2,
        name:"Jan",
        surname: "Kowalski",
        login: "JKowalski",
        email: "JKowalski@example.com",
        password: "1234qqwer",
        isAdmin: false,
    },
    {
        id: 3,
        name:"Mateusz",
        surname: "Nowak",
        email: "MNowak@example.com",
        login: "MNowak",
        password: "fsdfrfegvdvr",
        isAdmin: false,
    }
]

router.get('/', (req,res) => {
    res.send(users)
})

router.post('/', (req,res) => {
    console.log('posting data');
    let user = req.body;
    user.id = lastId + 1;
    users.push(user);
    res.send(users)
})

router.delete('/:id', (req,res) => {
    const {id} = req.params;
    const userById = users.find((user) => user.id == id)
    users = users.filter(user => user != userById)
    ;
    res.send(users)
})

router.get('/:id', (req,res) => {
    const {id} = req.params;
    const userById = users.find((user) => user.id == id)
    res.send(userById)
})

module.exports = router;